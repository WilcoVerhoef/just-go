import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/ogs/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  username: string = this.apiService.username;
  password: string;

  error = '';
  loading = false;

  constructor(
    private apiService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.apiService.logout();
  }

  login() {
    this.loading = true;

    this.apiService.login(this.username, this.password)
    .then( () => this.router.navigate(['/']) )
    .catch(
      error => {
        this.error = error.error.error_description;
        this.loading = false;
        this.password = '';
      }
    )
  }
}
