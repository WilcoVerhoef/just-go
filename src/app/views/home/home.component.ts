import { Component, OnInit } from '@angular/core';
import { faCog, faHandHoldingUsd, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  faCog = faCog;
  faGitlab = faGitlab;
  faHandHoldingUsd = faHandHoldingUsd;
  faInfoCircle = faInfoCircle;

  constructor() { }

  ngOnInit() {
  }

}
