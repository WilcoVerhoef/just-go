import { Component, OnInit, Input } from '@angular/core';
import { BoardSize } from 'src/app/ogs/interfaces/automatch';

@Component({
  selector: 'app-big-circle',
  templateUrl: './big-circle.component.html',
  styleUrls: ['./big-circle.component.scss']
})
export class BigCircleComponent implements OnInit {
  @Input() boardsize: BoardSize;

  get lines() {
    switch (this.boardsize) {
      case BoardSize["Quarter Board"]: return 2;
      case BoardSize["Half Board"]: return 3;
      case BoardSize["Full Board"]: return 4;
      default: return 4;
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
