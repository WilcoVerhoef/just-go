import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigCircleComponent } from './big-circle.component';

describe('BigCircleComponent', () => {
  let component: BigCircleComponent;
  let fixture: ComponentFixture<BigCircleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigCircleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
