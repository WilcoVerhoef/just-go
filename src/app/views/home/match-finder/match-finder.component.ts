import { Component, OnInit } from '@angular/core';
import { AutomatchService } from 'src/app/ogs/automatch.service';
import { Router } from '@angular/router';
import { TimeControlSystem } from 'src/app/ogs/interfaces/time-control';
import { AutomatchEntry, BoardSize, GameSpeed } from 'src/app/ogs/interfaces/automatch';
import { Ruleset } from 'src/app/ogs/interfaces/game-data';

@Component({
  selector: 'app-match-finder',
  templateUrl: './match-finder.component.html',
  styleUrls: ['./match-finder.component.scss']
})
export class MatchFinderComponent implements OnInit {
  boardsize : BoardSize = BoardSize["Quarter Board"];
  loading = false;

  boardsizes = Object.entries(BoardSize).map(
    boardsize => ({key: boardsize[0], value: boardsize[1]})
  );

  constructor(
    public automatchService: AutomatchService,
    private router: Router,
  ) { }

  ngOnInit() {

    // Go to game when a game is found...
  
    this.automatchService.matchStart.subscribe(match => {
      this.router.navigate(['/game/' + match.game_id]);
      this.loading = false;
    });
  }

  findMatch() {
    this.loading = true;

    let preferences: AutomatchEntry = {
      uuid: performance.now().toString(),
      size_speed_options: [{
        speed: GameSpeed.Live,
        size: this.boardsize,
      }],
      rules: {
        value: Ruleset.Japanese,
      },
      time_control: {
        value: { system: TimeControlSystem["Byo-yomi"] }
      },
      handicap: {
        value: 'enabled'
      }
    };

    this.automatchService.findMatch(preferences)
    .then(entry => {
      this.loading = false;
    });
  }

  cancelMatch() {
    this.loading = true;

    this.automatchService.cancelMatch()
    .then(entry => {
      this.loading = false;
    });
  }

}
