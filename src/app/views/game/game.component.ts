import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameService } from 'src/app/ogs/game.service';
import { switchMap, map, share, distinctUntilChanged, pluck, distinctUntilKeyChanged } from 'rxjs/operators';

enum Stone { BLACK = 1, WHITE = 2 };

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  data$ = this.route.paramMap.pipe(
    // Get the current game :id from the URL
    map(params => Number(params.get('id'))),

    // Connect to the game to get its data
    switchMap(id => this.game.connect(id)),

    // Make this Observable "hot"
    share()
  );

  board$ = this.data$.pipe(
    map(data => {

      // Create an empty board

      let board : Stone[][] = 
        Array(data.width).fill(null).map(()=>
        Array(data.height).fill(null));

      // Place all current stones

      data.moves.forEach((move, i) => {
        if (move[0] == -1) return;
        board[move[0]][move[1]] = i % 2 + 1;
      });

      return board;
    })
  );

  constructor(
    private route: ActivatedRoute,
    private game: GameService,
  ) {
    window['gameService'] = game;
    window['data$'] = this.data$;
  }

  ngOnInit() { }

}
