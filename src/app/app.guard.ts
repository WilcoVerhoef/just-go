import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './ogs/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {

  constructor(
    private apiService: AuthenticationService,
    private router: Router,
  ) {}

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
   
    let loggedIn = await this.apiService.testLogin();
    if (!loggedIn) this.router.navigate(['login']);

    return loggedIn;
  }
  
}
