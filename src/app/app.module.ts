import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { GameComponent } from './views/game/game.component';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoModule } from 'ngx-socket-io';
import { BigCircleComponent } from './views/home/big-circle/big-circle.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatchFinderComponent } from './views/home/match-finder/match-finder.component';
import { LoginFormComponent } from './views/home/login-form/login-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GameComponent,
    MatchFinderComponent,
    BigCircleComponent,
    MatchFinderComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot({
      url: 'https://online-go.com',
      options: {
        transports: ['websocket'],
        upgrade: false,
      }
    }),
    FontAwesomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
