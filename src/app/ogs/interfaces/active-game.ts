import { Player } from './player';
import { GamePhase } from './game-data';

export interface ActiveGame {
    id: number;
    phase: GamePhase;
    name: string;
    player_to_move: number;
    width: number;
    height: number;
    move_number: number;
    paused: number; // since
    private: boolean;
    black: Player;
    white: Player;
    time_per_move: number;
}