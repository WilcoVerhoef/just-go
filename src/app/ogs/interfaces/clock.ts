export interface PlayerTime {
    thinking_time: number;
    periods: number;
    period_time: number;
}

export interface Clock {
    game_id: number;
    current_player: number;
    black_player_id: number;
    white_player_id: number;
    title: string;
    last_move: number;
    expiration: number;
    black_time: PlayerTime;
    white_time: PlayerTime;

    now?: number;
    paused_since?: number;
    pause_delta?: number;
    expiration_delta?: number;
    stone_removal_mode?: boolean;
    stone_removal_expiration?: number;
}