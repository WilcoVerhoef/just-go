export interface Rating {
    rating: number,
    deviation: number,
    volatility: number,
    games_played: number,
}