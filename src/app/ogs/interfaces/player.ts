import { Rating } from './rating';

export interface Player {
    username: string;
    rank: number;
    professional: boolean;
    id: number;
    egf?: number;
    ratings?: {
        'overall': Rating;
    }
    accepted?: boolean;
}