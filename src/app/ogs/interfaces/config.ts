import { Rating } from './rating';

export interface Config {
    user: {
        anonymous: boolean,
        id: number,
        username: string,
        ratings: {
            'overall': Rating,
            'blitz': Rating,
            '13x13': Rating,
            'blitz-13x13': Rating,
            '9x9': Rating,
            'blitz-9x9': Rating,
            'live': Rating,
            'live-9x9': Rating,
            'live-13x13': Rating,
            '19x19': Rating,
            'live-19x19': Rating,
            'correspondence': Rating,
            'correspondence-9x9': Rating,
            'blitz-19x19': Rating
        }
        country: string,
        professional: boolean,
        ranking: number,
        can_create_tournaments: boolean,
        is_moderator: boolean,
        is_superuser: boolean,
        is_tournament_moderator: boolean,
        supporter: boolean,
        tournament_admin: boolean,
        ui_class: unknown,
        icon: string,
        email: string,
        email_validated: string,
        is_announcer: boolean,
    }
    chat_auth: string,
    incident_auth: string,
    notification_auth: string,
    user_jwt: string,
}