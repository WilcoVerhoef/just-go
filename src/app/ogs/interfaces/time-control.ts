export enum TimeControlSystem {
    'Byo-yomi' = 'byoyomi',
    'Fisher' = 'fischer',
    'Simple' = 'simple',
    'Canadian' = 'canadian',
}

export interface TimeControl {
    system: TimeControlSystem,
    main_time?: number,
    period_time?: number,
    periods?: number,
    pause_on_weekends?: boolean,
    initial_time?: number,
    time_increment?: number,
    max_time?: number,
    stones_per_period?: number,
    per_move?: number,
    time_control?: TimeControlSystem,
}