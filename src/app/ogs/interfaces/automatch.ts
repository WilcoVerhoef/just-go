
import { TimeControl } from './time-control';
import { Ruleset } from './game-data';

export enum GameSpeed {
  'Blitz' = 'blitz',
  'Live' = 'live',
  'Correspondence' = 'correspondence',
}

export enum BoardSize {
  'Quarter Board' = '9x9',
  'Half Board' = '13x13',
  'Full Board' = '19x19',
}

export type AutomatchCondition = 'required' | 'preferred' | 'no-preference';

export interface AutomatchEntry {
  uuid: string;
  timestamp?: number;
  size_speed_options: {
    speed: GameSpeed;
    size: BoardSize;
  }[];
  lower_rank_diff?: number;
  upper_rank_diff?: number;
  rules: {
      condition?: AutomatchCondition;
      value: Ruleset;
  };
  time_control: {
      condition?: AutomatchCondition;
      value: TimeControl;
  };
  handicap: {
      condition?: AutomatchCondition;
      value: 'enabled' | 'disabled';
  };
}