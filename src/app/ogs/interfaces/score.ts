import { BoardPositionListString } from './board-position';

export interface Score {
    total: number;
    stones: number;
    territory: number;
    prisoners: number;
    scoring_positions: BoardPositionListString;
    handicap: number;
    komi: number;
}