import { TimeControl } from './time-control';
import { Clock } from './clock';
import { Player } from './player';
import { Score } from './score';
import { BoardPositionListString } from './board-position';

export enum GamePhase {
    'Playing' = 'play',
    'Removing Stones' = 'stone removal',
    'Finished' = 'finished',
}

export enum GameOutcome {
    'r' = 'Resignation',
    'resign' = 'Resignation',
    'Resignation' = 'Resignation',
    'Disconnection' = 'Disconnection',
    'Stone Removal Timeout' = 'Stone Removal Timeout',
    'Timeout' = 'Timeout',
    'Cancellation' = 'Cancellation',
    'Disqualification' = 'Disqualification',
    'Moderator Decision' = 'Moderator Decision',
    'Abandonment' = 'Abandonment',
}

export enum Ruleset {
    'Japanese' = 'japanese',
    'Chinese' = 'chinese',
    'AGA' = 'aga',
    'Korean' = 'korean',
    'New Zealand' = 'nz',
    'Ing' = 'ing',
}

export type Move = [number, number, number];

export interface GameData {
    white_player_id: number;
    black_player_id: number;
    game_id: number;
    game_name: string;
    private: boolean;
    pause_on_weekends: boolean;
    players: {
        black: Player,
        white: Player
    };
    ranked: boolean;
    disable_analysis: boolean;
    handicap: number;
    komi: number;
    width: number;
    height: number;
    rules: Ruleset;
    time_control: TimeControl;
    meta_groups: number[];
    phase: GamePhase;
    initial_player: Player;
    moves: Move[];
    allow_self_capture: boolean;
    automatic_stone_removal: boolean;
    free_handicap_placement: boolean;
    aga_handicap_scoring: boolean;
    allow_ko: boolean;
    allow_superko: boolean;
    superko_algorithm: string;
    score_territory: boolean;
    score_territory_in_seki: boolean;
    score_stones: boolean;
    score_handicap: boolean;
    score_prisoners: boolean;
    score_passes: boolean;
    white_must_pass_last: boolean;
    opponent_plays_first_after_resume: boolean;
    strict_seki_mode: boolean;
    initial_state: {
        black: '';
        white: '';
    };
    start_time: number;
    original_disable_analysis: boolean;
    clock: Clock;
    winner: number;
    outcome: GameOutcome;
    end_time: number;

    // When game has ended / stone removal phase
    
    pause_control?: {
        'stone-removal': boolean;
    };
    paused_since?: number;
    removed?: BoardPositionListString;
    auto_scoring_done?: boolean;
    score?: {
        black: Score,
        white: Score,
    };
}