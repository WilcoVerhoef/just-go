export type BoardPositionString = string;
export type BoardPositionListString = string;

export class BoardPosition {

    // NOTE THE MISSING i
    private static readonly alphabet = '.abcdefghjklmnopqrstuvwxyz';

    constructor (
        public readonly x = -1,
        public readonly y = -1,
    ) {}

    toString() : BoardPositionString {
        return BoardPosition.alphabet.charAt(this.x) || '.' +
               BoardPosition.alphabet.charAt(this.y) || '.';
    }

    static fromString(pos: BoardPositionString) {
        return new BoardPosition(
            BoardPosition.alphabet.indexOf(pos[0]) || -1,
            BoardPosition.alphabet.indexOf(pos[1]) || -1,
        );
    }
}

export class BoardPositionList {

    constructor (
        public readonly items: readonly BoardPosition[]
    ) {}

    toString() : BoardPositionListString {
        return this.items.join('');
    }

    static fromString(posses: BoardPositionListString) {
        return new BoardPositionList(
            posses.match(/.{2}/g).map(BoardPosition.fromString)
        );
    }
}