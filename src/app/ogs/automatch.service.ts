import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { AutomatchEntry } from './interfaces/automatch';

@Injectable({
  providedIn: 'root'
})
export class AutomatchService {
  activeEntry: AutomatchEntry = null;

  readonly matchEntry: Observable<AutomatchEntry>;
  readonly matchCancel: Observable<AutomatchEntry>;
  readonly matchStart: Observable<{game_id:number, uuid:string}>;

  constructor(
    private socket: Socket,
  ) {

    this.matchEntry = this.socket.fromEvent('automatch/entry');
    this.matchStart = this.socket.fromEvent('automatch/start');
    this.matchCancel = this.socket.fromEvent('automatch/cancel');

    this.matchEntry.subscribe(entry => this.activeEntry = entry );
    this.matchStart.subscribe(() => this.activeEntry = null);
    this.matchCancel.subscribe(() => this.activeEntry = null);

    // Tell the server to send us entry updates
    this.socket.emit('automatch/list');
  }
  
  findMatch(preferences: AutomatchEntry) {
    this.socket.emit('automatch/find_match', preferences);
    return this.matchEntry.toPromise();
  }

  cancelMatch() {
    this.socket.emit('automatch/cancel', this.activeEntry.uuid);
    return this.matchCancel.toPromise();
  }
  
}
