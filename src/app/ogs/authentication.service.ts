import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Socket } from 'ngx-socket-io';
import { UserToken } from './interfaces/user-token';
import { Config } from './interfaces/config';
import { tap, mergeMap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private server = 'https://online-go.com';
  private readonly clientId     = 'just-go';
  private readonly clientSecret = 'just-go';

  constructor(
    private http: HttpClient,
    private socket: Socket,
  ) {
    this.authenticate();
  }

  get username() {
    let username = localStorage.getItem('username');
    if (!username) this.logout();
    return username || null;
  }

  private requestUserToken(username: string, password: string, refresh?: boolean) {
    
    let params = new HttpParams({
      fromObject: {
        client_id:      this.clientId,
        client_secret:  this.clientSecret,
        grant_type:     refresh ? 'refresh_token' : 'password',
        username:       username,
        ...( refresh
          ? { refresh_token: password }
          : { password:      password }
        )
      }
    });
    
    let request = this.http.post<UserToken>(this.server + '/oauth2/token/', params);

    return request.pipe(tap(response => {
      response.expires = Date.now() + response.expires_in * 500;
      localStorage.setItem('username', username);
      localStorage.setItem('userToken', JSON.stringify(response));
      setTimeout(this.getUserToken, response.expires_in * 600);
    }));
  }
  
  private getUserToken() {
    // Gets the cached userToken,
    // or when it's expired a refreshed userToken,
    // or when that's expired a newly requested userToken.

    let logoutError = () => (this.logout(), throwError('Not logged in!'));

    let username = localStorage.getItem('username');
    let rawUserToken = localStorage.getItem('userToken');

    if (!username) return logoutError();
    
    try {
      var userToken: UserToken = JSON.parse(rawUserToken);
    } catch {
      return logoutError();
    }

    if (!userToken) return logoutError();

    if (userToken.expires <= Date.now())
      return this.requestUserToken(username, userToken.refresh_token, true);
    
    return of(userToken);
  }

  getConfig() {
    return this.getUserToken().pipe(mergeMap(userToken =>
      this.http.get<Config>(this.server + '/api/v1/ui/config/', {
        headers: new HttpHeaders({'Authorization': 'Bearer ' + userToken.access_token})
      })
    ));
  }

  async getPlayerId() {
    // TODO use config cache

    let config = await this.getConfig().toPromise();
    return config.user.id;
  }

  async login(username: string, password: string) {
    let userToken = await this.requestUserToken(username, password).toPromise();
    this.authenticate();
    return userToken;
  }

  testLogin() {
    return new Promise<boolean>((resolve) => {
      this.getConfig().toPromise()
      .then(() => resolve(true))
      .catch(() => resolve(false));
    });
  }

  logout() {
    localStorage.removeItem('userToken');

    // TODO: check this
    // I think this works??
    // reauthentication should be necessary
    this.socket.disconnect();
    this.socket.connect();
  }

  authenticate() {
    // TODO make better

    return new Promise<boolean>((resolve) => {
      this.getConfig().toPromise()
      .then(config => {
        this.socket.emit('authenticate', {
          player_id: config.user.id,
          username: config.user.username,
          auth: config.chat_auth,
          jwt: config.user_jwt,
        });
        resolve(true);
      }).catch(() => {
        resolve(false);
      });
    });
  }
}
