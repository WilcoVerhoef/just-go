import { TestBed } from '@angular/core/testing';

import { AutomatchService } from './automatch.service';

describe('AutomatchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutomatchService = TestBed.get(AutomatchService);
    expect(service).toBeTruthy();
  });
});
