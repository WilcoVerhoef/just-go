import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Player } from './interfaces/player';
import { Score } from './interfaces/score';
import { AuthenticationService } from './authentication.service';
import { GameData, Move, GamePhase, GameOutcome } from './interfaces/game-data';
import { ActiveGame } from './interfaces/active-game';
import { BoardPosition, BoardPositionListString, BoardPositionList } from './interfaces/board-position';
import { expand, map } from 'rxjs/operators';

interface removed_stones_accepted {
  player_id: number;
  stones: string;
  players: {
    Black: Player,
    White: Player,
  };
  phase: GamePhase;
  score: {
    Black: Score,
    White: Score,
  };
  winner: number; // PlayerID
  outcome: GameOutcome;
  end_time: number;
}

@Injectable({
  providedIn: 'root'
})
export class GameService {
  activeGames: { [game_id: number] : ActiveGame } = {};

  constructor(
    private socket: Socket,
    private authenticationService: AuthenticationService,
  ) {
    
    // Keep track of active games

    this.socket.fromEvent<ActiveGame>('active_game').subscribe(game => {

      if (game.phase !== 'finished') this.activeGames[game.id] = game;
      else delete this.activeGames[game.id];
    });
  }

  connect(game_id: number) {

    // Ask the server to send us updates about this game

    this.authenticationService.getPlayerId().then(player_id => {
      this.socket.emit('game/connect', {
        game_id,
        player_id,
        chat: false,
      });
    });

    let move$ = this.getMove$(game_id);
    let phase$ = this.getPhase$(game_id);
    let removed$ = this.getRemovedStones$(game_id);
    let accepted$ = this.getAccepted$(game_id);

    // Listen for GameData updates

    return this.socket.fromEvent<GameData>(`game/${game_id}/gamedata`).pipe(

      // Expand Move updates onto data.moves
      // to keep it up to date!

      expand(data => move$.pipe(
        map(m => {
          data.moves[m.move_number] = m.move;
          return data;
        })
      )),

      // Expand phase updates onto data.phase
      // to keep it up to date!

      expand(data => phase$.pipe(
        map(p => {
          data.phase = p;
          return data;
        })
      )),

      // Expand removed stone updates onto
      // data.removed to keep it up to date!

      expand(data => removed$.pipe(
        map(r => {
          data.removed = r.all_removed;
          return data;
        })
      )),

      // Apply all updates from an
      // removed_stones_accepted event.

      expand(data => accepted$.pipe(
        map(update => Object.assign(data, update))
      )),

      // todo; are there any other data updates?
    );
  }

  private getMove$(game_id: number) {
    return this.socket.fromEvent<{
      game_id: number,
      move_number: number,
      move: Move
    }>(`game/${game_id}/move`);

    // Note: a move on -1 -1 means a pass
  }

  private getPhase$(game_id: number) {
    return this.socket.fromEvent<GamePhase>(`game/${game_id}/phase`);
  }

  private getRemovedStones$(game_id: number) {
    return this.socket.fromEvent<{
      removed?: Boolean,
      stones?: BoardPositionListString,
      all_removed?: BoardPositionListString
    }>(`game/${game_id}/removed_stones`);
  }

  private getAccepted$(game_id: number) {
    return this.socket.fromEvent<GameData>(`game/${game_id}/removed_stones_accepted`);
  }

  move(game_id: number, move: BoardPosition) {
    this.authenticationService.getPlayerId().then(player_id => {

      this.socket.emit(`game/move`, {
        game_id,
        player_id,
        move: move,
      });
    });

    return this.getMove$(game_id).toPromise();
  }

  pass(game_id: number) {
    return this.move(game_id, new BoardPosition(/*pass*/));
  }

  setRemoved(game_id: number, stones: BoardPositionList, removed: boolean) {
    this.authenticationService.getPlayerId().then(player_id => {

      this.socket.emit(`game/removed_stones/set`, {
        game_id,
        player_id,
        removed: Number(removed),
        stones: stones,
      });
    });

    return this.getRemovedStones$(game_id).toPromise();
  }

  acceptRemoved(game_id: number, stones: BoardPositionList, strict: boolean) {
    this.authenticationService.getPlayerId().then(player_id => {

      this.socket.emit(`game/removed_stones/accept`, {
        game_id,
        player_id,
        stones: stones,
        strict_seki_mode: strict,
      });
    });

    return this.getRemovedStones$(game_id).toPromise();
  }

  // game/:id/autoresign (got from server)
  // {game_id, playe_id, expiration: number}

  // game/:id/clearautoresign
  // also game/:id/clear_auto_resign
  // {game_id, player_id}

  // game/:id/conditional_moves
  // move number starting at 0 here
  // moves can probably be not null
  // {player_id, move_number, moves: null}

  // game/:id/removed_stones
  // can also be {} (first time)
  // {removed: number (actually interpretted as bool?), stones: 'letters', all_removed: 'letters'}

  // game/removed_stones/set TO SERVER
  // {game_id, player_id, removed: number, stones: 'letters'}

  // game/removed_stones/accept TO SERVER
  // {game_id, player_id, stones: 'letters', strict_seki_mode: bool}

  // game/:id/removed_stones_accepted
  // see interface removed_stones_accepted

  // game/:id/phase
  // "stone removal"
  // "finished"

  // todo: undo, cancel, resign, other special things
}
