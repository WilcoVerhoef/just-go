import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './views/game/game.component';
import { HomeComponent } from './views/home/home.component';
import { AppGuard } from './app.guard';
import { LoginFormComponent } from './views/home/login-form/login-form.component';
import { MatchFinderComponent } from './views/home/match-finder/match-finder.component';

const routes: Routes = [
  { path: 'game/:id', component: GameComponent, canActivate: [AppGuard] },
  { path: '', component: HomeComponent, children: [
    { path: 'find', component: MatchFinderComponent, canActivate: [AppGuard] },
    { path: 'login', component: LoginFormComponent },
    { path: '**', redirectTo: 'find' },
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
